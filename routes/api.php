<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\v1\Auth\LoginController;
use App\Http\Controllers\Api\v1\Auth\LogoutController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\Api\v1\CommentController;


Route::middleware(['localization'])->group(function() {
    Route::post('/Login' , LoginController::class);

    Route::middleware(['auth:api'])->group(function() {
        Route::resource('/Blog',BlogController::class);

        Route::post('/Comment',[CommentController::class,'store']);

        Route::get('/Logout',LogoutController::class);
    });
});
