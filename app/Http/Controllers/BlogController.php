<?php

namespace App\Http\Controllers;

use App\Http\Resources\BlogResource;
use App\Services\BlogService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Traits\Helpers\ApiResponse;

class BlogController extends Controller
{
    use ApiResponse;

    public function __construct(private BlogService $blogService) { }

    public function index() :JsonResponse
    {
        return $this->respondWithResourceCollection(BlogResource::collection($this->blogService->read()), trans('Crud.index',['object' => 'Blog' ]));
    }

    public function store(Request $request) :JsonResponse
    {
        return $this->respondWithResource(new BlogResource($this->blogService->create($request->all(),$request->hasFile('video'))),trans('Crud.store',['object' => 'Blog' ]));
    }

    public function update(Request $request , $id) :JsonResponse
    {
        if($this->blogService->update($request->all() , $id ,$request->hasFile('video')))
            return $this->respondSuccess(trans('Crud.update',['object' => 'Blog']));
        else
            return $this->respondSuccess(trans('Crud.',['object' => 'Blog' ]));
    }

    public function destroy($id) :JsonResponse
    {
        $this->blogService->delete($id);
        return $this->respondSuccess(trans('Crud.delete',['object' => 'Blog' ]));
    }
}
