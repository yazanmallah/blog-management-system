<?php

namespace App\Http\Controllers\Api\v1\Auth;

use App\Http\Controllers\Controller;
use App\Http\Resources\LoginResource;
use App\Services\AuthService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Traits\Helpers\ApiResponse;

class LoginController extends Controller
{
    use ApiResponse;

    public function __construct(private AuthService $authService) { }

    public function __invoke(Request $request) :JsonResponse
    {
        $data = [];

        if ($this->authService->Login($request->only(['email', 'password']), $request->get('remember_me', false), $data))
            return $this->respondWithResource(new LoginResource($data), trans('auth.login_success'));
        return $this->respondError(trans('auth.failed'));
    }
}
