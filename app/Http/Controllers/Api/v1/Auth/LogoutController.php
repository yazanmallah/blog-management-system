<?php

namespace App\Http\Controllers\Api\v1\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Traits\Helpers\ApiResponse;
use App\Services\AuthService;

class LogoutController extends Controller
{
    use ApiResponse;

    public function __construct(private AuthService $authService) { }

    public function __invoke(Request $request) :JsonResponse
    {
        if($this->authService->Logout())
            return $this->respondSuccess(trans('auth.logout_success'));
        else
            return $this->respondError(trans('auth.logout_failed'));
    }
}
