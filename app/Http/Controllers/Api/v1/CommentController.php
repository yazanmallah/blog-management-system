<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use Illuminate\Http\Request;
use App\Traits\Helpers\ApiResponse;
use App\Services\CommentService;
use App\Http\Resources\CommentResource;

class CommentController extends Controller
{
    use ApiResponse;

    public function __construct(private CommentService $commentService) { }

    public function store(Request $request)
    {
        return $this->respondWithResource(new CommentResource($this->commentService->create($request->all())),trans('Crud.store' , ['object' => 'Comment']));
    }
}
