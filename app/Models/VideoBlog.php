<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Traits\Relationships\VideoBlogRelationships;

class VideoBlog extends Model
{
    use HasFactory , VideoBlogRelationships;

    protected $fillable = ['url'];

    protected $table = 'video_blogs';
}
