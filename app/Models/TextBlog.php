<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Traits\Relationships\TextBlogRelationships;

class TextBlog extends Model
{
    use HasFactory , TextBlogRelationships;

    protected $fillable = ['details'];

    protected $table = 'text_blogs';
}
