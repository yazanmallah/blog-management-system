<?php

namespace App\Models;

use App\Traits\Relationships\CommentRelationships;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Comment extends Model
{
    use HasFactory , CommentRelationships;

    protected $fillable = ['text', 'blog_id'];
}
