<?php

namespace App\Models;

use App\Traits\Relationships\BlogRelationships;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @property mixed $id
 */
class Blog extends Model
{
    use HasFactory , BlogRelationships;

    protected $fillable = ['title', 'blogable_id', 'blogable_type', 'user_id'];
}
