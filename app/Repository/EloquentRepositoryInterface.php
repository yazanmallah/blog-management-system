<?php

namespace App\Repository;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

interface EloquentRepositoryInterface
{
    public function create(array $model) :?Model;
    public function read(array $relationships) :LengthAwarePaginator;
    public function update(array $payload , $id) :bool;
    public function delete(int $id) :bool;
}