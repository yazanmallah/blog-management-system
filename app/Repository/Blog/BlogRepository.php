<?php

namespace App\Repository\Blog;

use App\Models\Blog;
use App\Repository\BaseRepository;


class BlogRepository extends BaseRepository
{
    public function __construct(private Blog $blog)
    {
        parent::__construct($this->blog);
    }
}