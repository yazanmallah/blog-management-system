<?php

namespace App\Repository\Text;

use App\Models\TextBlog;
use App\Repository\BaseRepository;

class TextBlogRepository extends BaseRepository
{
    public function __construct(TextBlog $textBlog)
    {
        parent::__construct($textBlog);
    }
}