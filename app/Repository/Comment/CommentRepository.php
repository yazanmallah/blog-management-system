<?php

namespace App\Repository\Comment;

use App\Models\Comment;
use App\Repository\BaseRepository;
use App\Repository\EloquentRepositoryInterface;

class CommentRepository extends BaseRepository implements EloquentRepositoryInterface
{
    public function __construct( Comment $comment)
    {
        parent::__construct($comment);
    }
}