<?php

namespace App\Repository;

namespace App\Repository;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

class BaseRepository implements EloquentRepositoryInterface
{
    public function __construct(private Model $model) { }

    public function create(array $model) : ?Model {
        return $this->model
                    ->query()
                    ->create($model);
    }

    public function read(array $relationships) :LengthAwarePaginator {
        return $this->model
                    ->with($relationships)
                    ->paginate(15);
    }

    public function update(array $payload,$id) : bool {
        return $this->model
                    ->query()
                    ->update($payload);
    }

    public function delete(int $id) : bool {
        return $this->model
                    ->query()
                    ->find($id)
                    ->delete();
    }
}