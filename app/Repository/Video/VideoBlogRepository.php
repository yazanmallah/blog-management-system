<?php

namespace App\Repository\Video;

use App\Models\VideoBlog;
use App\Repository\BaseRepository;

class VideoBlogRepository extends BaseRepository
{
    public function __construct(VideoBlog $videoBlog)
    {
        parent::__construct($videoBlog);
    }
}