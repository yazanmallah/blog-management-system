<?php

namespace App\Services;

use App\Jobs\EmailPodcast;
use App\Models\Blog;
use App\Repository\Text\TextBlogRepository;
use App\Repository\Video\VideoBlogRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Repository\Blog\BlogRepository;


class BlogService
{
    private array $relationships = [];
    
    public function __construct(private BlogRepository $blogRepository , private VideoBlogRepository $videoBlogRepository , private TextBlogRepository $textBlogRepository) { }

    public function create(array $payload,bool $hasFile) :?Model
    {
        if($hasFile)
        {
            $path = $payload['video']->store('videos', ['disk' => 'video_blog']);
            $model = $this->videoBlogRepository->create([
                'url' => $path,
            ]);
        }
        else
        {
            $model = $this->textBlogRepository
                          ->create([
                            'details' => $payload['details']
                          ]);
        }

         $model->blog()
               ->create([
                   'user_id' => auth()->user()->id,
                   'title' => $payload['title']
               ]);
        dispatch(new EmailPodcast());


        return $model->load(['blog']);
    }

    public function read() :LengthAwarePaginator
    {
        return $this->blogRepository
                    ->read($this->relationships);
    }

    public function update(array $payload , int $id , $hasFile) :bool
    {
        $model = Blog::find($id);

        if($hasFile)
        {
            $path = $payload['video']->store('videos', ['disk' => 'video_blog']);
            $this->videoBlogRepository->update([
                'url' => $path,
            ]);
        }
        else
        {
            $this->textBlogRepository
                ->update([
                    'details' => $payload['details']
                ],$id);
        }

        return $this->blogRepository
                    ->update([
                        'title' => $payload['title']
                    ],$model->id);
    }

    public function delete($id) :bool
    {
        return $this->blogRepository
                    ->delete($id);
    }
}