<?php

namespace App\Services;

class AuthService
{
    public function Login(array $payload, $remember_me, &$data) :bool
    {
        if (auth()->attempt($payload, $remember_me)) {
            $data['user'] = auth()->user();

            $data['token'] = auth()->user()
                                   ->createToken('token')
                                   ->accessToken;

            return true;
        } else
            return false;
    }

    public function Logout() :bool
    {
        return auth()->user()
                     ->token()
                     ->revoke();
    }
}