<?php

namespace App\Services;

use App\Repository\Comment\CommentRepository;
use Illuminate\Database\Eloquent\Model;

class CommentService
{
    public function __construct(private CommentRepository $commentRepository) { }

    public function create(array $payload) :?Model
    {
        return $this->commentRepository
                    ->create($payload);
    }
}