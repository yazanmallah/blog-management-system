<?php

namespace App\Traits\Helpers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use JetBrains\PhpStorm\ArrayShape;

trait ApiResponse
{
    protected function apiResponse($data = [], $statusCode = 200, $headers = []) :JsonResponse
    {
        $result = $this->parseGivenData($data, $statusCode, $headers);

        return response()->json(
            $result['content'],
            $result['statusCode'],
            $result['headers']
        );
    }

    #[ArrayShape(["content" => "array", "statusCode" => "int|mixed", "headers" => "array"])]
    public function parseGivenData($data = [], $statusCode = 200, $headers = []) :array
    {
        $responseStructure = [
            'success' => $data['success'],
            'message' => $data['message'] ?? null,
            'data' => $data['data'] ?? null,
        ];
        if (isset($data['errors'])) {
            $responseStructure['errors'] = $data['errors'];
        }
        if (isset($data['status'])) {
            $statusCode = $data['status'];
        }

        if (isset($data['exception']) && ($data['exception'] instanceof Error || $data['exception'] instanceof Exception)) {
            if (config('app.env') !== 'production') {
                $responseStructure['exception'] = [
                    'message' => $data['exception']->getMessage(),
                    'file' => $data['exception']->getFile(),
                    'line' => $data['exception']->getLine(),
                    'code' => $data['exception']->getCode(),
                    'trace' => $data['exception']->getTrace(),
                ];
            }

            if ($statusCode === 200) {
                $statusCode = 500;
            }
        }
        if ($data['success'] === false) {
            if (isset($data['error_code'])) {
                $responseStructure['error_code'] = $data['error_code'];
            } else {
                $responseStructure['error_code'] = 1;
            }
        }
        return ["content" => $responseStructure, "statusCode" => $statusCode, "headers" => $headers];
    }

    protected function respondWithResource(JsonResource $resource, $message = null, $statusCode = 200, $headers = []) :JsonResponse
    {
        return $this->apiResponse(
            [
                'success' => true,
                'data' => $resource,
                'message' => $message
            ], $statusCode, $headers
        );
    }

    protected function respondWithResourceCollection(ResourceCollection $resourceCollection, $message = null, $statusCode = 200, $headers = []) :JsonResponse
    {
        return $this->apiResponse(
            [
                'success' => true,
                'data' => $resourceCollection,
                'message' => $message
            ], $statusCode, $headers
        );
    }

    protected function respondSuccess($message = '') : JsonResponse
    {
        return $this->apiResponse(['success' => true, 'message' => $message]);
    }

    protected function respondError($message, Exception $exception = null, int $statusCode = 400, int $error_code = 1): JsonResponse
    {
        report($exception);
        return $this->apiResponse(
            [
                'success' => false,
                'message' => $message ?? 'There was an internal error, Pls try again later',
                'exception' => $exception,
                'error_code' => $error_code
            ], $statusCode
        );
    }
}