<?php

namespace App\Traits\Relationships;

use App\Models\Blog;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait CommentRelationships
{
    public function blog() :BelongsTo
    {
        return $this->belongsTo(Blog::class);
    }
}