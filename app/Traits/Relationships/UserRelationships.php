<?php

namespace App\Traits\Relationships;

use Illuminate\Database\Eloquent\Relations\HasMany;

trait UserRelationships
{
    public function blogs() :HasMany
    {
        return $this->hasMany(Blog::class);
    }

}