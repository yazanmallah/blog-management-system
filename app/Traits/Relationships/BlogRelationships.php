<?php

namespace App\Traits\Relationships;

use App\Models\Comment;
use App\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;

trait BlogRelationships
{
    public function comments() :HasMany
    {
        return $this->hasMany(Comment::class);
    }

    public function user() :BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function blogable() :MorphTo
    {
        return $this->morphTo();
    }
}