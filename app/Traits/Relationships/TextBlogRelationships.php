<?php

namespace App\Traits\Relationships;

use App\Models\Blog;
use Illuminate\Database\Eloquent\Relations\MorphOne;

trait TextBlogRelationships
{
    public function blog() :MorphOne
    {
        return $this->morphOne(Blog::class, 'blogable');
    }
}