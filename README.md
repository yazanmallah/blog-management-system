<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About usage
- cloning the project  git clone https://gitlab.com/yazanmallah/blog-management-system.git
- ```php 
  installing dependencies composer install
- create .env file and fill its content with .env.example content


- add email info to your .env file


- run migration and seeder
- ```php
  php artisan migrate:fresh --seed 
- run create the encryption keys needed to generate secure access tokens
- ```` php 
  artisan passport:install
  
- create application key 
- ```php
  php artisan key:generate

- import postman collection attached with project file


## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
